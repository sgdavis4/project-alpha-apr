from django.shortcuts import render, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def project_list(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/project_list.html", context)


@login_required
def project_detail(request, id):
    project_instance = Project.objects.get(id=id)
    project_tasks = Task.objects.all()
    context = {
        "project_instance": project_instance,
        "project_tasks": project_tasks,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"create_project_form": form}
    return render(request, "projects/create_project.html", context)
